# kubernetes setup hutter.cloud

## Base Installation

Ubuntu 20.0.4 LTS with OpenSSH and microk8s

## Base Configuration

### ubuntu user and access
```bash
# copy ssh key and reset local ubuntu user password
# on local node
ssh-copy-id -i ~/.ssh/id_rsa.home ubuntu@node01

# on node01
passwd

# setup passwordless sudo
sudo /bin/sh -c 'echo ubuntu ALL=(ALL) NOPASSWD:ALL
 > /etc/sudoers.d/ubuntu'
```

### network configuration

For pihole we need to use port 53. as coredns exposes this as a nodeport we need to assign a second ip
to the node

To do this replace the values in 00-install-config.yaml
```yaml
# sudo vim /etc/netplan/00-installer-config.yaml
network:
  ethernets:
    eno1:
      dhcp4: false
      addresses:
        - 192.168.30.21/24
        - 192.168.30.22/24
      gateway4: 192.168.30.254
      nameservers:
        addresses:
          - 192.168.30.254
  version: 2
```

Then execute `sudo netplan apply`

### microk8s
Next, enable microk8s addons

```bash
# enable coredns, switch to internal dns resolution
# so internal doman names can be resolved
sudo microk8s enable dns:192.168.30.254
# enable rbac
sudo microk8s enable rbac
```

Now, add domain name 'k8s.hutter.cloud' to alt_names in ``/var/snap/microk8s/current/certs/csr.conf.template`.

```bash
sudo vim /var/snap/microk8s/current/certs/csr.conf.template
```

And allow low level node ports - dangerous in real installations!
Just simplifies the nginx-ingress setup.

```bash
sudo vim /var/snap/microk8s/current/args/kube-apiserver
# add this line,
# yeah givin that port range is quite dangerous
# but works well enough for my home setup
--service-node-port-range 53-32767
```

Restart microk8s (usually faster just to restart the whole node) 

```
# restart
sudo microk8s.stop
sudo microk8s.start
```

Add mount point for download volumes and monitoring volumes

```bash
sudo su
echo /dev/disk/by-uuid/8b2574ec-0fa9-4f92-ae04-13c87abe96b5 /mnt/data ext4 defaults 0 0 >> /etc/fstab
mkdir /mnt/data
mount /mnt/data
# usenet data
sudo mkdir -p /mnt/data/sabnzbd/complete
sudo mkdir -p /mnt/data/sabnzbd/incomplete
sudo mkdir -p /mnt/data/sabnzbd/logs
sudo chmod 777 /mnt/data/sabnzbd/complete
sudo chmod 777 /mnt/data/sabnzbd/incomplete
sudo chmod 777 /mnt/data/sabnzbd/logs
# monitoring data
sudo mkdir -p /mnt/data/prometheus/prometheus-db
sudo chmod 777 /mnt/data/prometheus/prometheus-db
sudo mkdir -p /mnt/data/loki/loki
sudo chmod 777 /mnt/data/loki/loki
sudo mkdir -p /mnt/data/elasticsearch/nodes
sudo chmod 777 /mnt/data/elasticsearch/nodes
sudo mkdir -p /mnt/data/influxdb/
sudo chown 777 /mnt/data/influxdb/
```

And finally install nfs utils for nfs pvcs

```bash
sudo apt-get install -y nfs-common
```

### kubectl

Create a kubeconfig file, change address from ip to `k8s.hutter.cloud`

```bash
sudo microk8s config
```


## Base ervice installation

helm is used to deploy the different services with exception of sealed secrets.

1. sealed-secrets:
1. cert-manager
2. cluster-issuer
3. external-dns
4. ingress-nginx
5. eck
6. jaeger-operator

```bash
for service in sealed-secrets cert-manager cluster-issuer ext-dns ingress-nginx eck jaeger-operator argocd-operator; do
  helm dependency update ./${service}
  helm upgrade --install -f ./${service}/values.yaml ${service} ./${service}
done
```

## sealed-secrets

To create a backup of the sealed secret master key

```bash
# create a base64 encoded string (single line !!!)
kubectl get secret -n kube-system -l sealedsecrets.bitnami.com/sealed-secrets-key -o yaml | base64 -w0
# add it to gopass
gopass set private/microk8s/sealed-secret/master.key
```

To recover the sealed secrets certificate do the following:

```bash
gopass private/microk8s/sealed-secret/master.key | base64 -d | kubectl apply -f -
kubectl delete pod -n kubesystem -l name=sealed-secrets-pod
```

## Services

Next, install the services we require.

```bash
# retrieve current public ip for external dns name 
for service in borgmatic certbot-nas external-names homeassistant keycloak kube-ops-view kubernetes-dashboard kubernetes-dashboard-proxy prometheus-monitoring usenet pihole; do
  helm dependency update ./${service}
  helm upgrade --install -f ./${service}/values.yaml ${service} ./${service}
done
```

